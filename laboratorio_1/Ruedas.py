#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# clase
class Ruedas():
    # constructor de la clase
    def __init__(self, rueda1, rueda2, rueda3, rueda4):
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4

    # metodo clase, asigna estado inicial a cada rueda de Auto
    def set_rueda1(self):
        self.rueda1 = 100
        return self.rueda1

    def get_rueda1(self):
        return self.rueda1

    def set_rueda2(self):
        self.rueda2 = 100
        return self.rueda2

    def get_rueda2(self):
        return self.rueda2

    def set_rueda3(self):
        self.rueda3 = 100
        return self.rueda3

    def get_rueda3(self):
        return self.rueda3

    def set_rueda4(self):
        self.rueda4 = 100
        return self.rueda4

    def get_rueda4(self):
        return self.rueda4
