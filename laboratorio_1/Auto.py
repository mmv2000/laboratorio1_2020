#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# libreria numeros aleatorios
import random
# se importa cada archivo objeto para crear Auto
from Motor import Motor
from Estanque import Estanque
from Velocimetro import Velocimetro
from Ruedas import Ruedas


# clase Auto
class Auto():
    # constructor de la clase con cada variable
    def __init__(self, motor, estanque, velocimetro,
                       rueda1, rueda2, rueda3, rueda4):
        self.motor = motor
        self.estanque = estanque
        self.velocimetro = velocimetro
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4

    # metodo de la clase, asigna la cilindrada del motor
    def set_motor(self):
        inicial = Motor(self.motor)
        inicial.tipo_motor()
        self.motor = inicial.get_motor()

    # metodo de la clase, muestra la cilindrada
    def get_motor(self):
        print("EL MOTOR DEL AUTO ES:", self.motor)

    # metodo de la clase, asigna tamaño del estanque
    def set_estanque(self):
        estanque = Estanque(self.estanque)
        estanque.set_estanque()
        self.estanque = estanque.get_estanque()

    # metodo de la clase, muestra el estado del estanque en cada ejecucion
    # si llega a 0, muestra que no hay bencina
    def get_estanque(self):
        if self.estanque <= 0:
            print("NO SE PUEDE SEGUIR AVANZANDO SIN BENCINA")
            exit()
        else:
            print("ESTANQUE ES DE: ", self.estanque)

    # metodo de la clase, velocidad en cada ejecución
    # aumenta o disminuye
    def set_velocimetro(self):
        velocimetro = Velocimetro(self.velocimetro)
        velocimetro.set_velocimetro()
        self.velocimetro = velocimetro.get_velocimetro()
        print("VELOCIDAD MÁXIMA REGISTRADA: ", self.velocimetro)

    # metodo de la clase, muestra velocidad marcada
    def get_velocimetro(self):
        return self.velocimetro

    # metodo de la clase, ruedas al 100% al crear Auto
    def set_ruedas(self):
        ruedas = Ruedas(self.rueda1, self.rueda2, self.rueda3, self.rueda4)
        rueda1 = ruedas.set_rueda1()
        self.rueda1 = rueda1
        rueda2 = ruedas.set_rueda2()
        self.rueda2 = rueda2
        rueda3 = ruedas.set_rueda3()
        self.rueda3 = rueda3
        rueda4 = ruedas.set_rueda4()
        self.rueda4 = rueda4
        print("RUEDAS EN ECELENTES CONDICIONES: {0}, {1}, {2}, {3}:".format
        (self.rueda1, self.rueda2, self.rueda3, self.rueda4))

    # metodo de la clase, disminuye litros del estanque
    # al arranque
    def arranque(self):
        self.estanque = self.estanque - (self.estanque / 100)
        print("EL ESTANQUE AHORA ES: ", self.estanque)

    # metodo de la clase, dado el motor muestra variable:
    # tiempo, velocidad, distancia y desgaste en cada intervalo de segundos
    def desgaste_estanque(self):
        tiempo = random.randint(1, 10)
        velocidad = self.velocimetro

        if self.motor == "1.2":
            distancia = velocidad / tiempo
            desgaste = distancia / 20
            self.estanque = self.estanque - desgaste
            print("TIEMPO TRANSCURRIDO: ", tiempo)
            print("VELOCIDAD REGISTRADA:", velocidad)
            print("DISTANCIA RECORRIDA: ", distancia)
            print("DESGASTE DEL ESTANQUE: ", desgaste)
        else:
            distancia = velocidad / tiempo
            desgaste = distancia / 14
            self.estanque = self.estanque - desgaste
            print("TIEMPO TRANSCURRIDO: ", tiempo)
            print("VELOCIDAD REGISTRADA:", velocidad)
            print("DISTANCIA RECORRIDA: ", distancia)
            print("DESGASTE DEL ESTANQUE: ", desgaste)

    # metodo de la clase, muestra el desgaste acumulado de las 4 ruedas de Auto
    def desgaste_rueda1(self):
        deterioro1 = random.randint(1, 10)
        if self.rueda1 > 10:
            self.rueda1 = self.rueda1 - deterioro1
        else:
            self.rueda1 = 100
        print("DESGASTE RUEDA 1 FUE DE: ", deterioro1)
        print("ACTUAL RUEDA 1:", self.rueda1)
        return self.rueda1

    def desgaste_rueda2(self):
        deterioro2 = random.randint(1, 10)
        if self.rueda2 > 10:
            self.rueda2 = self.rueda2 - deterioro2
        else:
            self.rueda2 = 100
        print("DESGASTE RUEDA 2 FUE DE: ", deterioro2)
        print("ACTUAL RUEDA 2: ", self.rueda2)
        return self.rueda2

    def desgaste_rueda3(self):
        deterioro3 = random.randint(1, 10)
        if self.rueda3 > 10:
            self.rueda3 = self.rueda3 - deterioro3
        else:
            self.rueda3 = 100
        print("DESGASTE RUEDA 3 FUE DE: ", deterioro3)
        print("ACTUAL RUEDA 3: ", self.rueda3)
        return self.rueda3

    def desgaste_rueda4(self):
        deterioro4 = random.randint(1, 10)
        if self.rueda4 > 10:
            self.rueda4 = self.rueda4 - deterioro4
        else:
            self.rueda4 = 100
        print("DESGASTE RUEDA 4 FUE DE: ", deterioro4)
        print("ACTUAL RUEDA: ", self.rueda4)
        return self.rueda4
