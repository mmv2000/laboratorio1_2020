#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# libreria numeros aleatorios
import random


# clase
class Motor():
    # constructor de la clase
    def __init__(self, motor):
        self.motor = motor

    # metodo clase, asigna el tipo de motor
    # puede ser 1.2 o 1.6
    def tipo_motor(self):
        self.motor = random.choice(["1.2", "1.6"])

    def get_motor(self):
        return self.motor
