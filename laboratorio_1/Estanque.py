#!/usr/bin/env python
# -*- coding: utf-8 -*-


# clase
class Estanque():
    # constructor de la clase
    def __init__(self, estanque):
        self.estanque = estanque

    # metodo clase, tamaño del estanque
    def set_estanque(self):
        self.estanque = 32

    def get_estanque(self):
        return self.estanque
