#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# se importa el archivo principal: Auto
from Auto import Auto

# main
if __name__ == '__main__':
    # booleano
    a = int(input("presione 1 para encender el auto: "))
    # variables que seran objetos para Auto
    motor_final = None
    estanque = 0
    velocimetro = 0
    rueda1 = 0
    rueda2 = 0
    rueda3 = 0
    rueda4 = 0
    # se crea un objeto a partir de la clase Auto
    auto = Auto(motor_final, estanque, velocimetro,
                rueda1, rueda2, rueda3, rueda4)
    # llama al metodo de la clase Auto, segun corresponda
    auto.set_motor()
    auto.set_estanque()
    auto.set_ruedas()
    # encender auto, mostrando condicion de c/objeto
    while a > 0:
        auto.get_motor()
        auto.get_estanque()
        auto.set_velocimetro()
        auto.arranque()
        auto.desgaste_estanque()
        auto.desgaste_rueda1()
        auto.desgaste_rueda2()
        auto.desgaste_rueda3()
        auto.desgaste_rueda4()
        a = int(input("presione 1 para encender el auto: "))
