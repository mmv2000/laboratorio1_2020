#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# clase
class Velocimetro():

    def __init__(self, velocimetro):
        self.velocimetro = velocimetro

    # metodo clase, velocidad aleatoria entre 1 y 120 Km
    def set_velocimetro(self):
        self.velocimetro = random.randint(1, 120)

    def get_velocimetro(self):
        return self.velocimetro
